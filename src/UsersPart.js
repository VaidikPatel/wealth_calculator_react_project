import React from 'react';
import './index.css';
import ShowUsers from './ShowUsers';
import ShowTotalWealth from './ShowTotalWealth';
import {useSelector} from 'react-redux';

function UsersPart() {
    
    const users = useSelector(state => state.users);
    const isShowTotalWealth = useSelector(state => state.isShowTotalWealth);
    

    return (
        <div className="column2" id="main1">
            <div className="item1">
                <h2>Person</h2>
                <h2>Wealth</h2>
            </div>
            { (users.length >0) && <ShowUsers  /> }
            { isShowTotalWealth && <ShowTotalWealth />}
        </div>
    );

    //Convert "UserPart" Component from 'using JSX' to 'without JSX' version
    // return  React.createElement("div", 
    //                             {className: "column2",id: "main1"},
    //                             React.createElement("div", 
    //                                                 {className: "item1"},
    //                                                 React.createElement("h2",
    //                                                                     null,
    //                                                                     "Person"
    //                                                                     ),
    //                                                 React.createElement("h2",
    //                                                                     null,
    //                                                                     "Wealth"
    //                                                                     )
    //                                                 ), 
    //                             users.length > 0 && React.createElement(ShowUsers, 
    //                                                                     {users: users}
    //                                                                     ), 
    //                             isShowTotalWealth && React.createElement(ShowTotalWealth,
    //                                                                     {totalWealth: totalWealth}
    //                                                                     )
    //                             );
}

export default React.memo(UsersPart);