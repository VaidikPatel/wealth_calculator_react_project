import React from 'react';
import './index.css';

function Title() {
    return (
        <h1 className="body-title" children="Wealth Calculator" />
    );
}


export default React.memo(Title);