import React, { useEffect } from 'react';
import _map from 'lodash/map';
import './App.css';

function useGiphy(query) {
    
    const [results,setResults] = React.useState([]);
    const [isLoading,setLoading] = React.useState(false);

    async function fetchData() {

        try {
            setLoading(true);
            const response = await fetch(`https://api.giphy.com/v1/gifs/search?api_key=oiYMYjzsosSEJvMSxGyqvK1RJiZDy64m&q=${query}&limit=50&offset=0&rating=g&lang=en`);
            const receivedData = await response.json();
            const listOfVideos = _map(receivedData.data,(item) => {
                return item.images.preview.mp4;
            });
            setResults(listOfVideos);
            setLoading(false);
        } catch(err) {
            alert(err);
        }
        
    }

    useEffect(() => {
        if(query !== '') {
            fetchData();
        }
    },[query]);

    return [results,isLoading];
}




function Shop() {

    const [search,setSearch] = React.useState('');
    const [query,setQuery] = React.useState('');

    function handleOnSubmit(e) {

        e.preventDefault();
        setQuery(search);
    }
    function handleUpdateSearch(e) {

        e.preventDefault();
        setSearch(e.target.value);
    }

    const [results,isLoading] = useGiphy(query); 


    const defaultStatement = <h1>Find your favorite Gifs !!!</h1> ;
    const loadingStatement = <h1>Loading ...</h1> ;
    const displayResultStatement = _map(results,item => <video key={item} src={item} autoPlay={true} loop={true} /> ) ;
    return (
        <div>
        <h1>Shop Page !!!</h1>
        <form onSubmit={handleOnSubmit}>
                <input 
                    value={search} 
                    placeholder="Search Gifs ...." 
                    onChange={handleUpdateSearch}
                />
                <button type="submit" >Submit</button>
        </form>
        { (query === '') ? defaultStatement : (isLoading) ? loadingStatement : (results.length) ? displayResultStatement  : <h1>Sorry No Gifs related to "{query}"</h1>}
        </div>
    );
}

export default Shop;