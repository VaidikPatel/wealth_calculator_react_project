import React from 'react';
import './index.css';
import {useSelector} from 'react-redux';

function ShowTotalWealth() {

    const totalWealth = useSelector(state => state.totalWealth);

    return (
        <div className="finalResult">
            <div>Total Wealth :</div> 
            <div>{`$ ${(totalWealth).toLocaleString('en-US')}`}</div>
        </div>
    );
} 

export default React.memo(ShowTotalWealth);