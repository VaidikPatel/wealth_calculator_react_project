import React from 'react';
import _map from 'lodash/map';
import './index.css';
import {useSelector} from 'react-redux';

function ShowUsers() {
    
    const users = useSelector(state => state.users);

    return (
            _map(users,(user) => {
            return (
                <div className="person" key={user.name} >
                        <div>{user.name}</div> 
                        <div>{`$ ${(user.money).toLocaleString('en-US')}`}</div> 
                </div>
            );
        })
        
    );
}


export default React.memo(ShowUsers);