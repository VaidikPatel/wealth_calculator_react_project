import React from 'react';
import './App.css';
import Title from './Title';
import Main from './Main';
import {createStore} from 'redux';
import allReducers from './reducers/allReducers';
import {Provider} from 'react-redux';

const myStore = createStore(  allReducers, 
                            window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

function Home() {
  return (
    <>
    <Provider store={myStore}>
        <Title />
        <Main />
    </Provider>
    </>
  );
}

export default Home;