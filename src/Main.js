import React from 'react';
import './index.css';
import ButtonsPart from './ButtonsPart';
import UsersPart from './UsersPart';

function Main() {
    
    return (
        <div className="parent">
            <ButtonsPart />
            <UsersPart />
        </div>
    );
}

export default React.memo(Main);