const usersReducer = (state = [],action) => {
    switch(action.type){
        case 'ADDUSER':
            return [...state , action.payload];
        case 'SETUSERS':
            return action.payload;
        default :
            return state;
    }
};

export default usersReducer;