const isShowTotalWealthReducer = (state = false,action) => {
    switch(action.type){
        case 'SHOWTOTALWEALTH':
            return true;
        case 'NOTSHOWTOTALWEALTH':
            return false;
        default :
            return state;
    }
};

export default isShowTotalWealthReducer;