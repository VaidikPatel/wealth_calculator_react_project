import usersReducer from "./users";
import isShowTotalWealthReducer from "./isShowTotalWealth";
import totalWealthReducer from "./totalWealth";
import {combineReducers} from 'redux';


const allReducers = combineReducers({
    users : usersReducer,
    isShowTotalWealth : isShowTotalWealthReducer,
    totalWealth : totalWealthReducer,
});

export default allReducers;