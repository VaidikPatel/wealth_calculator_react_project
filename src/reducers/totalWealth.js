const totalWealthReducer = (state = 0,action) => {
    switch(action.type){
        case 'SETTOTALWEALTH':
            return action.payload;
        default :
            return state;
    }
};

export default totalWealthReducer;