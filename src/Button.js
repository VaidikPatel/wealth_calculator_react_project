import React from 'react';
import './index.css';

function Button(props) {

    const {id,description,action} = props;
    
    return (
        <button 
            id={id} 
            children={description} 
            onClick={action} 
            className="buttonStyle"
        />
    );
}

export default React.memo(Button);