import React from 'react';
import './index.css';
import Button from './Button';
import _map from 'lodash/map';
import _filter from 'lodash/filter';
import _reduce from 'lodash/reduce';
import _orderBy from 'lodash/orderBy';
import {setShowTotalWealth, setNotShowTotalWealth, setUsers, addUser, setTotalWealth} from './actions/allActions';
import { useSelector,useDispatch } from 'react-redux';

const MILLION = 1e6; 

function ButtonsPart() {

    const users = useSelector(state => state.users);
    const dispatch = useDispatch();

    async function getRandomUserFromAPI() {
        
        dispatch(setNotShowTotalWealth());
        
        try {
            
            const response = await fetch('https://randomuser.me/api');
            const data = await response.json();
            const userdetail = data.results[0];
            const username = `${userdetail.name.first} ${userdetail.name.last}`;
            const wealth = Math.floor( Math.random() * 1e6 * 2);
            const userObj = {
                name : username,
                money : wealth
            };
            dispatch(addUser(userObj));
        } catch(err) { 
            alert(err);
        }
    }

    function doubleMoney() { 
        
        dispatch(setNotShowTotalWealth());
        const tempUsers = _map(users, (user) => ({...user , money : user.money * 2 }) ); // first one destructuring of obj and 2nd key:value pair is override on already having key:value pair 
        dispatch(setUsers(tempUsers));
    }
    function showOnlyMillionaires() {

        dispatch(setNotShowTotalWealth());
        const tempUsers = _filter(users, (user) => (user.money >= MILLION));
        dispatch(setUsers(tempUsers));
    }

    function sortDescendingByWealth() {
        
        dispatch(setNotShowTotalWealth());
        const tempUsers =_orderBy([...users],['money'],['desc']);
        dispatch(setUsers(tempUsers));
    }

    function calculateTotalWealth() {
        const currtotalWealth = _reduce(users, (sum,user) => sum += user.money, 0);
        dispatch(setTotalWealth(currtotalWealth));
        dispatch(setShowTotalWealth());
    }
    
    return (
        <div className = "column1" id = "main1">
            <Button 
                id="add-user" 
                description="Add User 👱‍♂️" 
                action={getRandomUserFromAPI}
            /> 
            <Button 
                id="double-money" 
                description="Double Money 💰" 
                action={doubleMoney} 
            />
            <Button 
                id="show-only-millionaries" 
                description="Show Only Millionaries 💵" 
                action={showOnlyMillionaires} 
            />
            <Button 
                id="sort-by-riches" 
                description="Sort By Richest ↓" 
                action={sortDescendingByWealth} 
            />   
            <Button 
                id="calculate-entire-wealth" 
                description="Calculate Entire Wealth 🤑" 
                action={calculateTotalWealth} 
            />   
        </div>
    );
}

export default React.memo(ButtonsPart);