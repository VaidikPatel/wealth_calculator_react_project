export const addUser = (newUser) => {
    return {
        type : 'ADDUSER',
        payload : newUser
    };
};

export const setUsers = (users) => {
    return {
        type : 'SETUSERS',
        payload : users
    };
};

export const setShowTotalWealth = () => {
    return {
        type : 'SHOWTOTALWEALTH'
    };
};

export const setNotShowTotalWealth = () => {
    return {
        type : 'NOTSHOWTOTALWEALTH'
    };
};

export const setTotalWealth = (totalWealth) => {
    return {
        type : 'SETTOTALWEALTH',
        payload : totalWealth
    };
};

